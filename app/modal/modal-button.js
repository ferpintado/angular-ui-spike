app.factory('ProductsFactory', ['BBYCONFIG', '$http', function(BBYCONFIG, $http) {

  var url = BBYCONFIG.api.productEndpoint;
  var cache = {};
  var category = "all";

  return {
    all: function() {
      return $.ajax({
        url: url,
        type: 'get',
        jsonpCallback: 'products',
        dataType: 'jsonp',
        cache: true
      });

    }
  };
}]);

app.directive('bbyWizard', ['BBYCONFIG', function(BBYCONFIG){
    return{
      restrict: 'E',
      transclude: true,      
      templateUrl: BBYCONFIG.root + '/modal/views/bby-wizard.html',
      link: function(scope, element, attrs, controller){
        scope.showNavigation = attrs.navigation;
        scope.currentSection = 1;        
        scope.finishButton = false;
        scope.totalSections = attrs.steps;

        if (typeof attrs.onFinish !== "undefined"){
            scope.finishButton = true;
          }

        scope.prevSection = function(){
          if( scope.currentSection > 0){
            scope.currentSection--;
          }
        }

        scope.getCurrentStep = function(){
          return scope.currentSection;
        }

        scope.nextSection = function(){
          if(scope.currentSection < scope.totalSections){
            scope.currentSection++;
          }
        }

        scope.onFinish = function(){
          if (scope.finishButton){
            scope.$eval(attrs.onFinish);
          }
        };

      }

    }

}]);
app.directive('bbyWizardSection', ['BBYCONFIG', function(BBYCONFIG){
    return{
      restrict: 'E',
      scope:false,
      templateUrl : function(element, attributes) {
          return  BBYCONFIG.root + attributes.template || "section.html";
      },
      link: function(scope, element, attrs, controller){
        if ( typeof attrs.onEnter !== typeof undefined && attrs.onEnter !== false){
          scope.$eval(attrs.onEnter);
        }
        
      }

    }

}]);

app.controller('MainCtrl', ['BBYCONFIG', '$scope', 'ProductsFactory', '$modal', 'ngDialog', function(BBYCONFIG, $scope, ProductsFactory,  $modal, ngDialog) {

  $scope.buttonLabel = 'Open modal';
  $scope.products = [];

  $scope.openModal = function() {
    $modal({
      scope: $scope,
      show:true,
      template: BBYCONFIG.root + '/modal/views/modal.html'
    }); 
    return false;
  };

  $scope.openModal2 = function(){
    ngDialog.open({ 
      template: BBYCONFIG.root + '/modal/views/modal2.html',
      scope: $scope,
    });
  }

  $scope.getProducts = function() {
    ProductsFactory.all().done(function(data){
      $scope.products = data.products;
      $scope.$digest();
    });
  };

  $scope.finishAction = function(){
    alert('This is the end');
  };

}]);

