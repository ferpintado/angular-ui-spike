var hostname = 'http://www.bestbuy.ca';
// (function(){
  var app = angular.module('app', ['mgcrea.ngStrap.modal', 'ngAnimate', 'ngDialog'])
  .constant('BBYCONFIG', {	
    root : "/app", 
    api: {
	    hostname: hostname,
	    productEndpoint: hostname + '/api/v2/json/search',
	  },
  });
// })();

